<%@ include file="/WEB-INF/pages/include.jsp" %>
<html>
<head>
<title>Bienvido al vocal ${inte.nombre}</title>
<link href="<c:url value='/css/bootstrap.min.css'/>" rel="stylesheet" media="screen">
<link href="<c:url value='/css/vocal.css'/>" rel="stylesheet">
<link href="<c:url value='/img/fav.ico'/>" rel="icon" type="image/png"  />
</head>
<body>
    <script src="<c:url value='/js/jquery-2.2.0.min.js'/>"></script>
    <script src="<c:url value='/js/bootstrap.min.js'/>"></script>
    
    <div class="container">
        <div class="row">
            <div class="col-md-12 center-block">
                <h1>Ya est&aacute;s adentro del sitio</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>${inte.nombre}, Ahora que sabemos que tu apellido es ${inte.apellido}, te vamos de dejar entrar...</p>
            </div>
        </div>
    </div>
    
    
 	
</body>
</html>