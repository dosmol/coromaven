/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coro.controller;

import com.coro.entidad.Integrante;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.aop.scope.ScopedProxyUtils.*;

/**
 *
 * @author martin
 */
@Controller
public class PrincipalController {

    @RequestMapping("/vocalConSonante")
    public ModelAndView vocalConSonante() {
        ModelAndView model = new  ModelAndView("welcome");
        model.addObject("msg", "Hola, estás dentro del sitio principal del vocal");
        
        return model;
    }
}
