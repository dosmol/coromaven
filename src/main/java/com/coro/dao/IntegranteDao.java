/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coro.dao;

import com.coro.entidad.Integrante;
import java.util.List;

/**
 *
 * @author martin
 */
public interface IntegranteDao {
    public boolean save (Integrante inte);
    public List<Integrante> buscar();
    public Integrante buscar(Integer id);
}
