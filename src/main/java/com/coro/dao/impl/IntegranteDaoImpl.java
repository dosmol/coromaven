/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coro.dao.impl;

import com.coro.dao.IntegranteDao;
import com.coro.entidad.Integrante;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author martin
 */
public class IntegranteDaoImpl implements IntegranteDao{
    private SessionFactory sessionFactory;
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    public boolean save(Integrante inte) {
        try{
        Session session = this.sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(inte);
        tx.commit();
        session.close();
        return true;
        }catch(Exception e){
            return false;
        }
    }

    public List<Integrante> buscar() {
        List<Integrante> integrantes = new ArrayList<Integrante>();
        try{
            Session session = this.sessionFactory.openSession();
            integrantes = session.createQuery("SELECT i from Integrante").list();
            session.close();
            return integrantes;
        }catch(Exception e){
            return integrantes;
        }
    }

    public Integrante buscar(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
